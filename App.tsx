import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { FiHome, FiUser } from 'react-icons/fi';

import Home from './src/screens/Home'
import Profile from './src/screens/Profile';
import Feed from './src/screens/Feed';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

function HomeTabs() {
  return(
    <Tab.Navigator tabBarOptions={{showLabel: false, activeTintColor: "red", inactiveBackgroundColor: "#363636"}}>
      <Tab.Screen name="Home" component={Home} 
                  options={{tabBarIcon: ({color, size}) => 
                  (<FiHome color={color} size={size} />)
                  }} />
      <Tab.Screen name="Profile" component={Profile} 
                  options={{tabBarIcon: ({color, size}) => 
                  (<FiUser color={color} size={size} />)
                  }} />
    </Tab.Navigator>
  )
}

function MenuDrawer() {
  return (
    <Drawer.Navigator screenOptions={{headerShown: true, headerStyle: {backgroundColor: "#9970d0"}}}>
      <Drawer.Screen name="MyApp" component={HomeTabs} />
      <Drawer.Screen name="Feed" component={Feed} />
    </Drawer.Navigator>
  )
}

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator >
        <Stack.Screen name="Menu" component={MenuDrawer} options={{headerShown: false}} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

